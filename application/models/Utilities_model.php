<?php

class Utilities_model extends CI_Model{

	public function addUtility($sessionID, $utilityName, $utilityType, $mobileNum){

		// $sessionID = '4ojqqsgf2gmbfkuk40854favtiab6b5d';
		// $utilityName = 'Ashish Stores';
		// $utilityType = 'General Stores';
		// $mobileNum = '7028132747';

		$q = $this->db->select('userID, entityID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;

		if($userID){

			$data = array(
				'residentID' => $userID,
				'utilityType' => $utilityType,
				'utilityName' => $utilityName,
				'mobileNum' => $mobileNum,
				'entityID' => $entityID
			);

			$q1 = $this->db->insert('utilities', $data);

			$id = $this->db->insert_id();

			echo json_encode(array('utilityID' => $id));
		}
	}

	public function getUtilities($sessionID){

		// $sessionID = '4ojqqsgf2gmbfkuk40854favtiab6b5d';

		$q = $this->db->select('userID, entityID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;
		$result = array();

		if($userID){
			$q1 = $this->db->select('*')
							->from('utilitylinks')
							->where('apartmentID', $entityID)
							->get();

			$utilitiesArray = $q1->result_array();

			for($i = 0; $i < $q1->num_rows(); $i++){

				$q2 = $this->db->select('storeName, proprietorName, mobileNum, secondaryMobNum, delivery, utilityID')
								->from('utilities')
								->where('utilityID', $utilitiesArray[$i]['utilityID'])
								->get();

				$result[$i] = $q2->row();

			}

			echo json_encode(array('utilities' => $result, 'num' => $q1->num_rows()), JSON_FORCE_OBJECT);
		}
	}

	public function getOrders($sessionID, $utilityID){

		// $sessionID = 'ettuanmuvvqfrov1cpju99jouvsd41t2';
		// $utilityID = 3;

		$q = $this->db->select('userID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){
			$q1 = $this->db->select('*')
							->from('orders')
							->where('utilityID', $utilityID)
							->where('userID', $userID)
							->get();

			$result = $q1->result();

			$num = $q1->num_rows();

			echo json_encode(array('orders' => $result, 'num' => $num), JSON_FORCE_OBJECT);
		}
	}

	public function addOrder($sessionID, $paymentMethod, $orderType, $orderPhoto, $utilityID){

		// $sessionID = 'ettuanmuvvqfrov1cpju99jouvsd41t2';
		// $utilityID = 3;

		$q = $this->db->select('userID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){

			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$time =date('H:i:s',time());

			
			$data = array(
				'userID' => $userID,
				'utilityID' => $utilityID,
				'paymentMode' => $paymentMethod,
				'orderType' => $orderType,
				'date' => $date,
				'time' => $time,
				'amount' => 0
			);

			$q1 = $this->db->insert('orders', $data);

			$orderID =$this->db->insert_id();

			$decodedImage=base64_decode($orderPhoto);
			file_put_contents('./orders/'.$orderID.".JPG", $decodedImage);

			$this->load->model('Notifications_model');
			$this->Notifications_model->sendOrderNotif($utilityID);

			echo json_encode(array('orderID' => $orderID));
		}
	}

	public function getOrdersForUtility($sessionID){

		// $sessionID = 'eg29l18v1k9e2lb3to5ver15phkljnsh';

		$q = $this->db->select('utilityID')
					->from('utilities')
					->where('sessionID', $sessionID)
					->get();

		$utilityID = $q->row()->utilityID;

		if($utilityID){

			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());

			$q1 = $this->db->select('orders.*, useraccts.userName, useraccts.userMobNum, useraccts.flatNum, useraccts.wing, entities.entityName, useraccts.entityID')
						->from('orders')
						->join('useraccts', 'useraccts.userID = orders.userID', 'inner')
						->join('entities','entities.enitityID = useraccts.entityID', 'inner')
						->where('orders.utilityID', $utilityID)
						->where('date', $date)
						->get();

			$result = $q1->result();

			$num = $q1->num_rows();

			echo json_encode(array('orders' => $result, 'num' => $num), JSON_FORCE_OBJECT);

		}

	}

	public function uploadAmountForOrder($sessionID, $orderID, $amount){

		// $sessionID = 'u5i95qt2fnf00a67fkkd2ti7sas57m16';
		// $orderID = 18;
		// $amount =100;

		$q = $this->db->select('utilityID')
						->from('utilities')
						->where('sessionID', $sessionID)
						->get();

		$utilityID = $q->row()->utilityID;

		if($utilityID){
			$data = array(
				'amount' => $amount
			);

			$q1 = $this->db->where('orderID', $orderID)
							->update('orders', $data);

			$q2 = $this->db->select('userID')
							->from('orders')
							->where('orderID', $orderID)
							->get();

			$userID = $q2->row()->userID;

			$this->load->model('Notifications_model');
			$this->Notifications_model->sendOrderConfirmation($userID);

			echo json_encode(array('result' => 0));
							
		}
	}

	public function changeMobNum($sessionID, $mobNum){

		// $sessionID = 'b0q3ncgmsfm5cfqfqopndt9qfjvg5pm4';
		// $mobNum = '789';

		$q = $this->db->select('utilityID')
						->from('utilities')
						->where('sessionID', $sessionID)
						->get();

		$utilityID = $q->row()->utilityID;

		if($utilityID){

			$data = array(
				'mobileNum' => $mobNum
			);

			$q1 = $this->db->where('utilityID', $utilityID)
							->update('utilities', $data);

			echo json_encode(array('result' => 0));
		}
	}

	public function changeSecondaryMobNum($sessionID, $secondaryMobNum){

		$q = $this->db->select('utilityID')
						->from('utilities')
						->where('sessionID', $sessionID)
						->get();

		$utilityID = $q->row()->utilityID;

		if($utilityID){

			$data = array(
				'secondaryMobNum' => $secondaryMobNum
			);

			$q1 = $this->db->where('utilityID', $utilityID)
							->update('utilities', $data);

			echo json_encode(array('result' => 0));
		}
	}
}