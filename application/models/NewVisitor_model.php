<?php

class NewVisitor_model extends CI_Model{

	public function uploadVisitorDetails($visitorName,$visitorType,$purposeOfMeet,$organisation,$IDProof,$IDNum,$userName,$userID,$employeeDeptID,$mobNum,$visitorImage,$sessionID,$prebookID, $IDImage, $wing, $flatNum){

		  // $sessionID='3kggk7hu0aph72hae54u89grmfm7rfqe';

		  // $userID=127;
		  // $visitorName='Ash Pawar';

		/*	
			getting security acct ID
		*/
		$q1=$this->db->select('securityAcctID, entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securitAcctID=$q1->row()->securityAcctID;
		$entityID = $q1->row()->entityID;


		/*
			if security ID exists
		*/
		if($securitAcctID){
			/*
				getting date and in-time
			*/
			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$inTime =date('H:i:s',time());

			/*
				creating insert array
			*/
			$data=array(
				'userID'=>$userID,
				'userName'=>$userName,
				'employeeDeptID'=>$employeeDeptID,
				'date'=>$date,
				'inTime'=>$inTime,
				// 'outTime'=>"",
				'purposeOfMeet'=>$purposeOfMeet,
				'organisation'=>$organisation,
				'IDProof'=>$IDProof,
				'IDNo'=>$IDNum,
				'mobNum'=>$mobNum,
				'visitorName'=>$visitorName,
				'type'=>$visitorType,
				'entityID' => $entityID,
				'flatNum' => $flatNum,
				'wing' => $wing
				);

			/*
				inserting in db
			*/
			$q=$this->db->insert('newVisitors',$data);

			/*
				getting insert id
			*/
			$id=$this->db->insert_id();

			/*
				saving image
			*/
			$decodedImage=base64_decode($visitorImage);
			file_put_contents('./newVisitors/'.$id.".JPG", $decodedImage);

			$decodedImage=base64_decode($IDImage);
			file_put_contents('./IDImage/'.$id.".JPG", $decodedImage);


			/*
				if it was prebooked
			*/
			if($prebookID!=0){
				/*
					filling update array for prebook entry
				*/
				$prebookData=array(
					'visitorID'=>$id
					);

				/*
					updating perbook entry with visitor id
				*/
				$q2=$this->db->where('prebookID',$prebookID)
							->update('prebook',$prebookData);

			}

			if($userID != 0){
				$this->load->model('Notifications_model');
				$this->Notifications_model->sendNewVisitorPushNotif($userID, $visitorName, $id);

				//$this->Notifications_model->sendNotifToEmployee($userID,$visitorName);
				//echo "here";
			}

			$error=$this->db->error();

			echo json_encode(array('inTime'=>$inTime,'visitorID'=>$id),JSON_FORCE_OBJECT);
	
		}else{
			echo json_encode(array('visitorID'=>0,'inTime'=>""));
		}
	}

	public function exit($visitorID,$sessionID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $visitorID=30;


		/*
			getting security acct ID
		*/
		$q1=$this->db->select('securityAcctID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securitAcctID=$q1->row();

		/*
			if security ID exists
		*/
		if($securitAcctID){

			/*
				getting exit date and time
			*/
			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$outTime =date('H:i:s',time());

			/*
				exit time array
			*/
			$exitArray=array(
				'outTime'=>$outTime
				);

			/*
				updating visitor entry with exit time
			*/
			$q=$this->db->where('newVisitorID',$visitorID)
						->update('newVisitors',$exitArray);

			$error=$this->db->error();

			echo json_encode(array('outTime'=>$outTime),JSON_FORCE_OBJECT);
		}else{
			echo json_encode(array('outTime'=>""),JSON_FORCE_OBJECT);
		}
	}

	public function fetchNewVisitors($sessionID,$IDMax){

		// $sessionID='5f8d39c809f344c1d02439c128f9b933bd0d9d6e';
		// $IDMax=0;

		/*
			getting employee ID
		*/
		$q=$this->db->select('userID')
					->from('userAccts')
					->where('sessionID',$sessionID)
					->get();

		$userID=$q->row()->userID;
		

		/*
			if employee ID exists
		*/
		if($userID){
			$q1=$this->db->select('*')
						->from('newVisitors')
						->where('userID',$userID)
						->where('newVisitorID > ',$IDMax)
						->get();

			$result=$q1->result();

			$num=$q1->num_rows();

			echo json_encode(array('newVisitors'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
		}
	}

	public function exitVisitorByEmployee($sessionID, $visitorID){

		//  $sessionID='asdf';
		// $visitorID = 132;

		$q = $this->db->select('userID')
					->from('userAccts')
					->where('sessionID',$sessionID)
					->get();

		$userID = $q->row()->userID;

		if($userID){

			date_default_timezone_set('Asia/Kolkata');

			$datetime = date('Y-m-d H:i:s', time());

			$exitData = array(
				'exitByUser'=>$datetime,
				'exited'=>1
			);

			$q1 = $this->db->where('newVisitorID',$visitorID)
						->update('newVisitors',$exitData);

			$error = $this->db->error();

			echo json_encode(array('result'=>0));
		}
	}

	public function checkExited($sessionID, $visitorID){

		//  $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $visitorID = 132;

		$q1=$this->db->select('securityAcctID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securitAcctID=$q1->row();

		/*
			if security ID exists
		*/
		if($securitAcctID){
				$q = $this->db->select('exited, exitByUser')
							->from('newVisitors')
							->where('newVisitorID',$visitorID)
							->get();

				$exited = $q->row()->exited;
				$exitByUser = $q->row()->exitByUser;

				echo json_encode(array('exited'=>$exited, 'exitByUser'=>$exitByUser),JSON_FORCE_OBJECT);
		}
	}

	public function getNewVisitorsForSecurity($sessionID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $IDMax = 156;
		
		$q = $this->db->select('securityAcctID, entityID')
						->from('securityAccts')
						->where('sessionID',$sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){

			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());

			$q1 = $this->db->select('newvisitors.*, departments.departmentName')
							->from('newvisitors')
							->join('departments','newvisitors.employeeDeptID = departments.departmentID','left')
							->where('newvisitors.entityID',$entityID)
							->where('newvisitors.date',$date)
							->get();

			$result = $q1->result();
			$num_rows = $q1->num_rows();

			echo json_encode(array('newVisitors'=>$result, 'num'=>$num_rows),JSON_FORCE_OBJECT);
		}
	}

	public function checkRepeatedVisitor($sessionID, $mobNum){

		// $sessionID='3kggk7hu0aph72hae54u89grmfm7rfqe';
		// $mobNum = '7517409714';

		$q = $this->db->select('securityAcctID')
						->from('securityAccts')
						->where('sessionID',$sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;

		if($securityAcctID){
			$q1 =  $this->db->select('newVisitorID, visitorName, organisation, IDProof, IDNo, type')
							->from('newVisitors')
							->where('mobNum',$mobNum)
							->get();

			$result = $q1->row();

			if($q1->num_rows() >= 1)
				echo json_encode($result);
			else
				echo json_encode(array(),JSON_FORCE_OBJECT);


		}
	}
}