<?php

class Parking_model extends CI_Model{

	public function updateCount($sessionID, $count){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $count = 3;

		$q = $this->db->select('*')
						->from('securityAccts')
						->where('sessionID', $sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){

			$data = array(
				'count' => $count
				);

			$q1 = $this->db->where('entityID', $entityID)
							->update('parking', $data);

			echo json_encode(array('result'=>0));
		}
	}

	public function getCounter($sessionID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';

		$q = $this->db->select('*')
						->from('securityAccts')
						->where('sessionID', $sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){
			$q1 = $this->db->select('*')
							->from('parking')
							->where('entityID', $entityID)
							->get();

			$result = $q1->row();

			echo json_encode(array('parkingCount'=>$result));
		}
	}
}