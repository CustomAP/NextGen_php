<?php

class DailyVisitor_model extends CI_Model{

	public function addDailyVisitor($name,$organisation,$designation,$IDProof,$IDNum,$mobileNum,$image,$sessionID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';

		$q=$this->db->select('securityAcctID, entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securityAcctID=$q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){
			$data=array(
				'visitorName'=>$name,
				'IDProof'=>$IDProof,
				'IDNo'=>$IDNum,
				'mobNum'=>$mobileNum,
				'organisation'=>$organisation,
				'designation'=>$designation,
				'entityID'=>$entityID
				);

			$q1=$this->db->insert('dailyVisitors',$data);

			$id=$this->db->insert_id();

			/*
				saving image
			*/
			$decodedImage=base64_decode($image);
			file_put_contents('./dailyVisitors/'.$id.".JPG", $decodedImage);

			echo json_encode(array('dailyVisitorID'=>$id));
		}
	}

	public function inDailyVisitor($sessionID,$dailyVisitorID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $dailyVisitorID=7;

		$q=$this->db->select('securityAcctID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securityAcctID=$q->row()->securityAcctID;

		if($securityAcctID){
			/*
				getting date and in-time
			*/
			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$inTime =date('H:i:s',time());
			
			$data=array(
				'dailyVisitorID'=>$dailyVisitorID,
				'date'=>$date,
				'inTime'=>$inTime
				);

			$q1=$this->db->insert('dailyVisitorsEntry',$data);

			$dailyVisitorEntryID=$this->db->insert_id();

			echo json_encode(array('dailyVisitorEntryID'=>$dailyVisitorEntryID,'time'=>$inTime),JSON_FORCE_OBJECT);
		}
	}


	public function outDailyVisitor($sessionID,$dailyVisitorEntryID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $dailyVisitorEntryID=3;

		$q=$this->db->select('securityAcctID')
				->from('securityAccts')
				->where('sessionID',$sessionID)
				->get();

		$securityAcctID=$q->row()->securityAcctID;

		if($securityAcctID){
			/*
				getting date and in-time
			*/
			date_default_timezone_set('Asia/Kolkata');

			//$date = date('Y-m-d', time());
			$outTime =date('H:i:s',time());

			$data=array(
				'outTime'=>$outTime
				);

			$q1=$this->db->where('dailyVisitorEntryID',$dailyVisitorEntryID)
						->update('dailyVisitorsEntry',$data);

			echo json_encode(array('time'=>$outTime),JSON_FORCE_OBJECT);
		}
	}

	public function getDailyVisitors($sessionID, $IDMax){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';
		// $IDMax = 28;

		$q = $this->db->select('securityAcctID, entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){
			$q1 = $this->db->select('*')
							->from('dailyVisitors')
							->where('entityID',$entityID)
							->where('dailyVisitorID > ',$IDMax)
							->get();

			$dailyVisitors = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dailyVisitors'=>$dailyVisitors, 'num'=>$num), JSON_FORCE_OBJECT);
		}
	}

	public function getDailyVisitorLogs($sessionID){

		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';

		$q = $this->db->select('securityAcctID, entityID')
						->from('securityAccts')
						->where('sessionID',$sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		date_default_timezone_set('Asia/Kolkata');
		$date = date('Y-m-d', time());

		if($securityAcctID){
			$q1 = $this->db->select('dailyVisitorsentry.*, dailyVisitors.visitorName, dailyVisitors.entityID')
							->from('dailyVisitorsentry')
							->join('dailyVisitors','dailyVisitorsentry.dailyVisitorID = dailyVisitors.dailyVisitorID', 'left')
							->where('dailyvisitors.entityID', $entityID)
							->where('dailyvisitorsentry.date',$date)
							->get();

			$dailyvisitorLogs = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dailyvisitorLogs'=>$dailyvisitorLogs, 'num'=>$num),JSON_FORCE_OBJECT);
		}
	}
}