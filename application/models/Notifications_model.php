<?php

class Notifications_model extends CI_Model{

	public function sendNotifToEmployee($userID,$visitorName){
		//$apiKey= '35186266-c627-11e7-94da-0200cd936042';

		// echo $userID;
		// echo $visitorName;

		$q=$this->db->select('userMobNum')
					->from('useraccts')
					->where('userID',$userID)
					->get();

		$mobNum=$q->row()->userMobNum;

		//echo $mobNum;

		//echo function_exists('curl_version');

        $YourAPIKey='35186266-c627-11e7-94da-0200cd936042';
        $From="NEXGEN";
        $To=$mobNum;
        $TemplateName="EmployeeNotif";
        $VAR1=$visitorName;
        $VAR2="<VAR2>";
        $VAR3="<VAR3>";
        $VAR4="<VAR4>";
        $VAR5="<VAR5>";

        ### DO NOT Change anything below this line
        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $url = "https://2factor.in/API/V1/$YourAPIKey/ADDON_SERVICES/SEND/TSMS"; 
        $ch = curl_init(); 
        curl_setopt($ch,CURLOPT_URL,$url); 
        curl_setopt($ch,CURLOPT_POST,true); 
        curl_setopt($ch,CURLOPT_POSTFIELDS,"TemplateName=$TemplateName&From=$From&To=$To&VAR1=$VAR1&VAR2=$VAR2&VAR3=$VAR3&VAR4=$VAR4&VAR5=$VAR5"); 
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch); 

        $err = curl_error($ch);

		if ($err) {
  			//echo "cURL Error #:" . $err;
		}

        curl_close($ch);
	}

    public function sendDSPPushNotif($dspID){

        $q = $this->db->select('*')
                    ->from('dsplink')
                    ->where('dspID', $dspID)
                    ->get();

        $dspLinkArray = $q->result_array();

        // echo json_encode(array('$dspLink' => $dspLinkArray));

        for($i = 0; $i < $q->num_rows(); $i++){
            $q1 = $this->db->select('name, dspID')
                            ->from('dsp')
                            ->where('dspID', $dspLinkArray[$i]['dspID'])
                            ->get();

            $dspID = $q1->row()->dspID;
            $name = $q1->row()->name;

            $q2 = $this->db->select('playerID')
                            ->from('useraccts')
                            ->where('userID', $dspLinkArray[$i]['residentID'])
                            ->get();

            $playerID = $q2->row()->playerID;

            // echo json_encode(array('dspID'=> $dspID, 'name' => $name, 'playerID' => $playerID), JSON_FORCE_OBJECT);

            $content = array(
            "en" => 'Your ' . $dspLinkArray[$i]['work'] . " " . $name . " has arrived"
            );

            $heading = array(
            "en" => 'Daily Service Provider'
            );
        
              $fields = array(
                'app_id' => "3d80db87-056b-4c8f-9777-ca0fe83a0dd8",
                'include_player_ids' => array($playerID),
                'data' => array("dspID" => $dspID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10
                );
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NmFmMTRhODMtMzkwZC00MWUxLTgxZDItMTUxNGIxMzVmNzA1'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
        
    
            // print("\n\nJSON received:\n");
            // print($response);
            // print("\n");
        }
    }

    public function sendNewVisitorPushNotif($userID, $visitorName, $visitorID){

        $q = $this->db->select('playerID, entityID')
                        ->from('useraccts')
                        ->where('userID', $userID)
                        ->get();

        $playerID = $q->row()->playerID;
        $entityID = $q->row()->entityID;

        $q1 = $this->db->select('entityType')
                        ->from('entities')
                        ->where('enitityID', $entityID)
                        ->get();

        $entityType = $q1->row()->entityType;

         $content = array(
            "en" => $visitorName . " has arrived to visit you."
            );

          $heading = array(
            "en" => 'New Visitor'
            );
        
           $ch = curl_init();

        if($entityType == 2){

              $fields = array(
                'app_id' => "3d80db87-056b-4c8f-9777-ca0fe83a0dd8",
                'include_player_ids' => array($playerID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10,
                'big_picture' => 'http://www.nextgenentrycard.com/newVisitors/'.$visitorID.'.JPG'
                );

              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NmFmMTRhODMtMzkwZC00MWUxLTgxZDItMTUxNGIxMzVmNzA1'));
          }else if($entityType == 1){

                 $fields = array(
                'app_id' => "f0206481-92ea-4a0e-be76-42015c54b03a",
                'include_player_ids' => array($playerID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10,
                'big_picture' => 'http://www.nextgenentrycard.com/newVisitors/'.$visitorID.'.JPG'
                );

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic ZDU2MTUwNzMtNTc0ZC00YmYyLWExMTUtYzY3NjQzYjI3YjA2'));
          }
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
           
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);

            // print("\n\nJSON received:\n");
            // print($response);
            // print("\n");
    }

    public function sendExitDSPNotif($dspID){
         $q = $this->db->select('*')
                    ->from('dsplink')
                    ->where('dspID', $dspID)
                    ->get();

        $dspLinkArray = $q->result_array();

        // echo json_encode(array('$dspLink' => $dspLinkArray));

        for($i = 0; $i < $q->num_rows(); $i++){
            $q1 = $this->db->select('name, dspID')
                            ->from('dsp')
                            ->where('dspID', $dspLinkArray[$i]['dspID'])
                            ->get();

            $dspID = $q1->row()->dspID;
            $name = $q1->row()->name;

            $q2 = $this->db->select('playerID')
                            ->from('useraccts')
                            ->where('userID', $dspLinkArray[$i]['residentID'])
                            ->get();

            $playerID = $q2->row()->playerID;

            // echo json_encode(array('dspID'=> $dspID, 'name' => $name, 'playerID' => $playerID), JSON_FORCE_OBJECT);

            $content = array(
            "en" => 'Your ' . $dspLinkArray[$i]['work'] . " " . $name . " has left"
            );

            $heading = array(
            "en" => 'Daily Service Provider'
            );
        
              $fields = array(
                'app_id' => "3d80db87-056b-4c8f-9777-ca0fe83a0dd8",
                'include_player_ids' => array($playerID),
                'data' => array("dspID" => $dspID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10
                );
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NmFmMTRhODMtMzkwZC00MWUxLTgxZDItMTUxNGIxMzVmNzA1'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
        
    
            // print("\n\nJSON received:\n");
            // print($response);
            // print("\n");
        }
    }

    public function sendOrderNotif($utilityID){
        $q2 = $this->db->select('playerID')
                            ->from('utilities')
                            ->where('utilityID', $utilityID)
                            ->get();

            $playerID = $q2->row()->playerID;

            // echo json_encode(array('dspID'=> $dspID, 'name' => $name, 'playerID' => $playerID), JSON_FORCE_OBJECT);

            $content = array(
            "en" => 'You have received one new Order'
            );

            $heading = array(
            "en" => 'New Order'
            );
        
              $fields = array(
                'app_id' => "0df989f3-f52b-46d3-9c1e-6bb55bc7e48b",
                'include_player_ids' => array($playerID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10
                );
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic MDY5M2VmYWMtZTNmMC00ZjgwLWIyMGQtNmY3MjE0NmU3ZGFl'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);

            // print("\n\nJSON received:\n");
            // print($response);
            // print("\n");
    }

    public function sendOrderConfirmation($userID){
        $q2 = $this->db->select('playerID')
                            ->from('useraccts')
                            ->where('userID', $userID)
                            ->get();

            $playerID = $q2->row()->playerID;

            // echo json_encode(array('dspID'=> $dspID, 'name' => $name, 'playerID' => $playerID), JSON_FORCE_OBJECT);

            $content = array(
            "en" => 'Your order has been confirmed'
            );

            $heading = array(
            "en" => 'Order Confirmation'
            );
        
              $fields = array(
                'app_id' => "3d80db87-056b-4c8f-9777-ca0fe83a0dd8",
                'include_player_ids' => array($playerID),
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10
                );
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NmFmMTRhODMtMzkwZC00MWUxLTgxZDItMTUxNGIxMzVmNzA1'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);

            // print("\n\nJSON received:\n");
            // print($response);
            // print("\n");
    }

    public function sos($sessionID){
        // $sessionID = 'ma0kprh9083i2omlrhlhkdrnvbisc1d7';

        $q = $this->db->select('entityID')
                        ->from('securityaccts')
                        ->where('sessionID', $sessionID)
                        ->get();

        $entityID = $q->row()->entityID;

        if($entityID){
            $q1 = $this->db->select('playerID')
                            ->from('useraccts')
                            ->where('entityID', $entityID)
                            ->get();

            $playerID = $q1->result_array();

            // echo $playerID;

            $playerIDArray = array();
            foreach ( $playerID as $Items ) 
                $playerIDArray[] = $Items["playerID"];

            $content = array(
            "en" => 'Security guard has sent SOS Signal'
            );

            $heading = array(
            "en" => 'SOS'
            );
        
              $fields = array(
                'app_id' => "3d80db87-056b-4c8f-9777-ca0fe83a0dd8",
                'include_player_ids' => $playerIDArray,
                'contents' => $content,
                'headings' => $heading,
                'priority' => 10,
                'android_channel_id' => "cf73dda6-bb87-458d-98ed-36c3af6062c1"
                );
        
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic NmFmMTRhODMtMzkwZC00MWUxLTgxZDItMTUxNGIxMzVmNzA1'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
        }
    }
}