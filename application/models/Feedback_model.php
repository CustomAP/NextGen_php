<?php

class Feedback_model extends CI_Model{

	public function sendFeedback($sessionID, $feedback){

		// $sessionID='c474b3a0f26ab2429444300faba83c7c9e73e87d';
		// $feedback = 'test';

		$q = $this->db->select('userID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){

			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$time =date('H:i:s',time());

			$data = array(
				'userID' => $userID,
				'feedback' => $feedback,
				'date' => $date,
				'time' => $time
			);

			$q1 = $this->db->insert('feedback', $data);

			echo json_encode(array('result' => 0));
		}
	}
}

?>