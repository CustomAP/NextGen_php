<?php

class Login_model extends CI_Model {

	public function securityLogin($entityID, $securityID, $password) {

		 $serverPassword='Ashish';
		// $password='Ashish';
		// $entityID=1;
		// $securityID=1;

		if(strcmp($password,$serverPassword)==0){

			// $entityID=1;
			// $securityID=1;

			$sessData=array(
				'entityID'=>$entityID,
				'securityID'=>$securityID
				);

			$this->session->set_userdata($sessData);

			$sessionID=$this->session->session_id;

			$data=array(
				'entityID'=>$entityID,
				'securityID'=>$securityID,
				'sessionID'=>$sessionID
				);

			$q=$this->db->insert('securityAccts',$data);	

			$q1=$this->db->select('securityAcctID')
						->from('securityAccts')
						->where('sessionID',$sessionID)
						->get();

			$ID=$q1->row()->securityAcctID;

			echo json_encode(array('ID'=>$ID,'sessionID'=>$sessionID));
		} else {
			echo json_encode(array('ID'=>0));
		}
	}

	public function employeeLogin($mobNum,$departmentID,$entityID,$userName, $playerID){

		// $mobNum='7028132747';
		// $departmentID=1;
		// $entityID=1;
		// $userName="Ashish";
        $userID = 0;

		$q1 = $this->db->select('userID')
					->from('useraccts')
					->where('userMobNum',$mobNum)
                    ->where('userType', 1)
					->get();


        if($q1->num_rows() != 0)
		  $userID = $q1->row()->userID;


		if(!$userID){
			$sessData=array(
				'userName'=>$userName,
				'entityID'=>$entityID,
				'departmentID'=>$departmentID
				);

			$this->session->set_userdata($sessData);

			$sessionID=$this->session->session_id;

			$data=array(
				'entityID'=>$entityID,
				'departmentID'=>$departmentID,
				'userName'=>$userName,
				'userMobNum'=>$mobNum,
                'userType'=>1,
				'sessionID'=>$sessionID,
                'playerID' => $playerID
				);

			$q=$this->db->insert('useraccts',$data);

		}else{
			$q2 = $this->db->select('sessionID')
							->from('useraccts')
							->where('userID',$userID)
							->get();

			$sessionID = $q2->row()->sessionID;

			$data=array(
				'entityID'=>$entityID,
				'departmentID'=>$departmentID,
				'userName'=>$userName,
                'playerID' => $playerID
				);

			$q3 = $this->db->where('userID',$userID)
                            ->update('useraccts',$data);
		}

		echo json_encode(array('sessionID'=>$sessionID));
	}

	public function rr($dirname){
		if (is_dir($dirname))
        	   $dir_handle = opendir($dirname);
		 if (!$dir_handle)
		      return false;
		 while($file = readdir($dir_handle)) {
	 	      if ($file != "." && $file != "..") {
	        	    if (!is_dir($dirname."/".$file))
	   		              unlink($dirname."/".$file);
		            else
		                 $this->rr($dirname.'/'.$file);
		       }
		 }
		 closedir($dir_handle);
		 rmdir($dirname);
		 return true;
	}

	public function changeProfilePic($sessionID,$image){

		// $sessionID='56fa20baeab1d91178a7f7f3657ed22eeaac82dd';

		$q=$this->db->select('userID')
					->from('useraccts')
					->where('sessionID',$sessionID)
					->get();

		$userID=$q->row()->userID;

		if($userID){
			//$imageName='ash';
			$imageName=$userID;

			$decodedImage=base64_decode($image);
			file_put_contents('./employees/'.$imageName.".JPG", $decodedImage);

			echo json_encode(array('ResultSet'=>0));
		}
	}

    public function adminLogin($sessionID, $password){
        $serverPassword = 'Ashish';
        // $password = 'Ashish';
        // $entityID = 1;
        // $sessionID = 'abcd';

        if(strcmp($serverPassword, $password) == 0){

            $q2 = $this->db->select('entityID, userID')
                            ->from('useraccts')
                            ->where('sessionID', $sessionID)
                            ->get();

            $entityID = $q2->row()->entityID;

            $userID = $q2->row()->userID;

            $sessData=array(
                'entityID'=>$entityID,
                'userID'=>$userID
                );

            $this->session->set_userdata($sessData);

            $adminSessionID=$this->session->session_id;

            $data=array(
                'entityID'=>$entityID,
                'sessionID'=>$adminSessionID,
                'userID'=>$userID
                );

            $q=$this->db->insert('adminAccts',$data); 

            $q1 = $this->db->select('adminID')
                        ->from('adminAccts')
                        ->where('sessionID',$adminSessionID)
                        ->get();

            $id = $q1->row()->adminID;   

            echo json_encode(array('adminID'=>$id,'adminSessionID'=>$adminSessionID), JSON_FORCE_OBJECT);
        }else{
            echo json_encode(array('adminID'=>0, 'adminSessionID'=>""), JSON_FORCE_OBJECT);
        }

    }

    public function residentLogin($userName, $entityID, $mobNum, $wing, $flatNum, $playerID){

        // $userName = 'Ash';
        // $entityID = 2;
        // $mobNum = '1597536482';
        // $wing = 'A';
        // $flatNum = '112';


        $userID = 0;

        $q1 = $this->db->select('userID')
                    ->from('useraccts')
                    ->where('userMobNum',$mobNum)
                    ->where('userType', 2)
                    ->get();

        if($q1->num_rows())
            $userID = $q1->row()->userID;


        if(!$userID){
            $sessData=array(
                'userName'=>$userName,
                'entityID'=>$entityID,
                'wing'=>$wing,
                'flatNum'=>$flatNum
                );

            $this->session->set_userdata($sessData);

            $sessionID=$this->session->session_id;

            $data=array(
                'entityID'=>$entityID,
                'wing'=>$wing,
                'flatNum'=>$flatNum,
                'userName'=>$userName,
                'userMobNum'=>$mobNum,
                'sessionID'=>$sessionID,
                'userType'=>2,
                'playerID' => $playerID
                );

            $q=$this->db->insert('useraccts',$data);

            $userID = $this->db->insert_id();

        }else{
            $q2 = $this->db->select('sessionID')
                            ->from('useraccts')
                            ->where('userID',$userID)
                            ->get();

            $sessionID = $q2->row()->sessionID;

            $data=array(
                'entityID'=>$entityID,
                'wing'=>$wing,
                'flatNum' => $flatNum,
                'userName'=>$userName,
                'playerID' => $playerID
                );

            $q3 = $this->db->where('userID',$userID)
                            ->update('useraccts',$data);
        }

        $q3 = $this->db->select('securities.securityName')
                        ->from('entities')
                        ->join('securities','securities.securityID = entities.securityID', 'inner')
                        ->where('entities.enitityID', $entityID)
                        ->get();

        $securityName = $q3->row()->securityName;

        echo json_encode(array('sessionID'=>$sessionID, 'securityName' => $securityName, 'userID' => $userID), JSON_FORCE_OBJECT);
    }

    public function utilityLogin($storeName, $proprietorName, $mobNum, $secondaryMobNum, $gstNum, $delivery, $playerID){
        // $mobNum = '7028132747';
        $utilityID = 0;

        $q1 = $this->db->select('utilityID')
                        ->from('utilities')
                        ->where('mobileNum', $mobNum)
                        ->get();

        if($q1->num_rows())
        $utilityID = $q1->row()->utilityID;

        if($utilityID){
            $sessData = array(
                'storeName' => $storeName,
                'mobileNum' => $mobNum,
                'secondaryMobNum' => $secondaryMobNum
            );
            
            $this->session->set_userdata($sessData);

            $sessionID=$this->session->session_id;


            $data = array(
                'storeName' => $storeName,
                'proprietorName' => $proprietorName,
                'mobileNum' => $mobNum,
                'secondaryMobNum' => $secondaryMobNum,
                'gstNum' => $gstNum,
                'sessionID' => $sessionID,
                'delivery' => $delivery,
                'playerID' => $playerID
            );

            $q = $this->db->where('utilityID', $utilityID)
                            ->update('utilities', $data);

            echo json_encode(array('sessionID' => $sessionID));

        }else{

            $sessData = array(
                'storeName' => $storeName,
                'mobileNum' => $mobNum,
                'secondaryMobNum' => $secondaryMobNum
            );

            $this->session->set_userdata($sessData);

            $sessionID=$this->session->session_id;


            $data = array(
                'storeName' => $storeName,
                'proprietorName' => $proprietorName,
                'mobileNum' => $mobNum,
                'secondaryMobNum' => $secondaryMobNum,
                'gstNum' => $gstNum,
                'sessionID' => $sessionID,
                'delivery' => $delivery,
                'playerID' => $playerID
            );

            $q = $this->db->insert('utilities', $data);


            echo json_encode(array('sessionID'=> $sessionID));
        }
    }
}