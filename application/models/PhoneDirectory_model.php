<?php

class PhoneDirectory_model extends CI_Model{

	public function addDirectory($adminSessionID, $wing, $flatNum, $name, $mobNum){

		// $adminSessionID = 'hsilkm9tlr4ne0vdvv9fei694nj83stf';

		$q = $this->db->select('adminID, entityID')
						->from('adminaccts')
						->where('sessionID', $adminSessionID)
						->get();

		$adminID = $q->row()->adminID;
		$entityID = $q->row()->entityID;

		if($adminID){
			$data = array(
				'wing' => $wing,
				'flatNum' => $flatNum,
				'name' => $name,
				'mobNum' => $mobNum,
				'entityID' => $entityID
			);

			$q1 = $this->db->insert('directory', $data);

			$id = $this->db->insert_id();

			
			echo json_encode(array('directoryID' => $id));
		}
	}

	public function getDirectory($sessionID){

		// $sessionID = '4ojqqsgf2gmbfkuk40854favtiab6b5d';

		$q = $this->db->select('entityID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$entityID = $q->row()->entityID;

		if($entityID){
			$q1 = $this->db->select('*')
							->from('directory')
							->where('entityID', $entityID)
							->get();

			$result = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('directory' => $result, 'num' => $num), JSON_FORCE_OBJECT);
		}
	}
}