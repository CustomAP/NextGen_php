<?php

class Forum_model extends CI_Model{

	public function getForumItems($sessionID){

		// $sessionID = 'ettuanmuvvqfrov1cpju99jouvsd41t2';

		$q = $this->db->select('userID, entityID')
							->from('useraccts')
							->where('sessionID', $sessionID)
							->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;

		if($userID){
			$q1 = $this->db->select('forum.*, useraccts.userID, useraccts.userName, useraccts.userMobNum, useraccts.flatNum, useraccts.wing')
							->from('forum')
							->join('useraccts', 'useraccts.userID = forum.userID', 'inner')
							->where('entityID', $entityID)
							->get();

			$result = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('forum' => $result, 'num' => $num), JSON_FORCE_OBJECT);
		}
	}

	public function addForumItem($sessionID, $heading, $description, $poll, $option1, $option2){

		// $sessionID = 'ettuanmuvvqfrov1cpju99jouvsd41t2';
		// $heading = 'ash';
		// $description = 'asdfa';

		$q = $this->db->select('userID, entityID')
							->from('useraccts')
							->where('sessionID', $sessionID)
							->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;

		if($userID) {
			date_default_timezone_set('Asia/Kolkata');
			$date = date('Y-m-d', time());
			$time = date('H:i:s',time());

			$data = array(
				'heading' => $heading,
				'description' => $description,
				'userID' => $userID,
				'date' => $date,
				'time' => $time,
				'poll' => $poll,
				'option1' => $option1,
				'option2' => $option2
			);

			$q1 = $this->db->insert('forum', $data);

			echo json_encode(array('result' => 0));
		}
	}

	public function vote($sessionID, $forumID, $optionNum){

		// $sessionID ='i4usg4fumamoaatom9ftt7nioj972iaf';
		// $forumID = 5;
		// $optionNum = 1;

		$q = $this->db->select('userID')
							->from('useraccts')
							->where('sessionID', $sessionID)
							->get();

		$userID = $q->row()->userID;

		if($userID){

			$voteID = 0;

			$q1 = $this->db->select('voteID')
							->from('votes')
							->where('userID', $userID)
							->where('forumID', $forumID)
							->get();

			if($q1->num_rows() > 0)
				$voteID = $q1->row()->voteID;

			if($voteID){

				$data = array(
					'optionNum' => $optionNum
				);

				$q2 = $this->db->where('voteID', $voteID)
								->update('votes', $data);

			}else{
				$data = array(
					'forumID' => $forumID,
					'userID' => $userID,
					'optionNum' => $optionNum
				);

				$q2 = $this->db->insert('votes', $data);
			}

			echo json_encode(array('result' => 0));
		}

	}

	public function getPollResult($sessionID, $forumID){

		// $sessionID = 'i4usg4fumamoaatom9ftt7nioj972iaf';
		// $forumID = 5;

		$q = $this->db->select('userID')
							->from('useraccts')
							->where('sessionID', $sessionID)
							->get();

		$userID = $q->row()->userID;

		$q1 = $this->db->select('userID')
						->from('forum')
						->where('forumID', $forumID)
						->get();

		$forumUserID = $q1->row()->userID;

		if($forumUserID == $userID){

			$q2 = $this->db->select('votes.*, useraccts.userName, useraccts.wing, useraccts.flatNum')
							->from('votes')
							->join('useraccts', 'useraccts.userID = votes.userID', 'left')
							->where('forumID', $forumID)
							->get();

			$result = $q2->result();

			$num = $q2->num_rows();

			echo json_encode(array('result' => $result, 'num' => $num), JSON_FORCE_OBJECT);
		}

	}
} 
?>