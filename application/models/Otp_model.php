<?php

class Otp_model extends CI_Model {

	public function sendOTP($mobNum) {
		// $mobNum='7028132747';
		$apiKey='35186266-c627-11e7-94da-0200cd936042';

		$curl = curl_init();

		curl_setopt_array($curl, array(
  		CURLOPT_URL => "http://2factor.in/API/V1/".$apiKey."/SMS/".$mobNum."/AUTOGEN",
  		CURLOPT_RETURNTRANSFER => true,
  		CURLOPT_ENCODING => "",
 		CURLOPT_MAXREDIRS => 10,
  		CURLOPT_TIMEOUT => 30,
  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  		CURLOPT_CUSTOMREQUEST => "GET",
  		CURLOPT_POSTFIELDS => "",
  		CURLOPT_HTTPHEADER => array(
    		"content-type: application/x-www-form-urlencoded"
  			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
  			echo "cURL Error #:" . $err;
		} else {
  			echo $response;
		}
	}


	public function verifyOTP($otp,$sessionID) {
		// $otp='922202';
		// $sessionID='a0736c0c-b567-11e7-94da-0200cd936042';
		$apiKey='35186266-c627-11e7-94da-0200cd936042';

		$curl = curl_init();

		curl_setopt_array($curl, array(
 		CURLOPT_URL => "http://2factor.in/API/V1/".$apiKey."/SMS/VERIFY/".$sessionID."/".$otp,
  		CURLOPT_RETURNTRANSFER => true,
 		CURLOPT_ENCODING => "",
  		CURLOPT_MAXREDIRS => 10,
  		CURLOPT_TIMEOUT => 30,
  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  		CURLOPT_CUSTOMREQUEST => "GET",
  		CURLOPT_POSTFIELDS => "",
  		CURLOPT_HTTPHEADER => array(
    		"content-type: application/x-www-form-urlencoded"
  			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
  			echo "cURL Error #:" . $err;
		} else {
  			$ans=json_decode($response);
  			if(strcmp($ans->Status,"Success")==0)
				echo json_encode(array('ResultSet'=>0));
			else
				echo json_encode(array('ResultSet'=>1));
			//echo $response;
		}
	}
}