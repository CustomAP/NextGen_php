<?php

class Rooms_model extends CI_Model{

	public function getRooms($sessionID){

		$sessionID='c474b3a0f26ab2429444300faba83c7c9e73e87d';

		$q = $this->db->select('*')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;

		if($userID){
			$q1 = $this->db->select('*')
							->from('rooms')
							->where('entityID', $entityID)
							->get();

			$result = $q1->result();

			$num = $q1->num_rows();

			echo json_encode(array('rooms'=>$result, 'num' => $num), JSON_FORCE_OBJECT);
		}
	}

	public function getRoomBookings($sessionID, $date, $roomID){

		// $sessionID='c474b3a0f26ab2429444300faba83c7c9e73e87d';
		// $date = '2018-02-20';
		// $roomID = 1;

		$q = $this->db->select('*')
						->from('useracct')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;
		$entityID = $q->row()->entityID;

		if($userID){
			$q1 = $this->db->select('roombookings.*, useracct.userName')
							->from('roombookings')
							->join('useracct','roombookings.userID = useracct.userID','inner')
							->where('roombookings.date', $date)
							->where('roombookings.roomID', $roomID)
							->get();

			$result = $q1->result();

			$num = $q1->num_rows();

			echo json_encode(array('bookings' => $result, 'num' =>$num), JSON_FORCE_OBJECT);
		}

	}

	public function bookRoom($sessionID, $date, $roomID, $startTime, $endTime){

		// $sessionID='c474b3a0f26ab2429444300faba83c7c9e73e87d';
		// $date = '2018-02-20';
		// $roomID = 1;
		// $startTime = '14:00:00';
		// $endTime = '15:00:00';
		
		$q = $this->db->select('*')
						->from('useracct')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){
			$data = array(
				'date' => $date,
				'roomID' => $roomID,
				'startTime' => $startTime,
				'endTime' => $endTime,
				'userID' => $userID
				);

			$q1 = $this->db->insert('roombookings', $data);

			echo json_encode(array('result' => 0));
		}
	}
}		