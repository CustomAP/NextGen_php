<?php 

class Admin_model extends CI_Model{
	
	public function getStatistics($sessionID, $startDate, $endDate){
		// $startDate = '2017-03-16';
		// $endDate = '2018-03-17';
		// $sessionID = 'bpret31rko1maummam78fqtmg8hpinif';
		
		$q = $this->db->select('adminID, entityID')
					->from('adminAccts')
					->where('sessionID',$sessionID)
					->get();

		$adminID = $q->row()->adminID;
		$entityID = $q->row()->entityID;

		// echo $entityID;

		$i = $this->db->select('entityType')
						->from('entities')
						->where('enitityID', $entityID)
						->get();

		$entityType = $i->row()->entityType;

		if($adminID){
			$q1 = $this->db->select('count(prebookID) as prebookCount')
							->from('prebook')
							->where('visitorID != 0')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$prebookCount = $q1->row()->prebookCount - 0;   //converting into int

			$q2 = $this->db->select('count(newVisitorID) as newVisitorCount')
							->from('newVisitors')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$newVisitorCount = $q2->row()->newVisitorCount - $prebookCount;

			if($entityType == 1){

				$q3 = $this->db->select('count(dailyVisitorEntryID) as dailyVisitorsCount')
							->from('dailyvisitorsentry')
							->join('dailyvisitors','dailyvisitors.dailyVisitorID = dailyvisitorsentry.dailyVisitorID','inner')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

				$dailyVisitorsCount = $q3->row()->dailyVisitorsCount - 0;

			}else if($entityType == 2){

				$q3 = $this->db->select('count(dspEntryID) as dspCount')
							->from('dspentry')
							->join('dsp', 'dsp.dspID = dspentry.dspID','inner')
							->where('date >= ', $startDate)
							->where('date <= ', $endDate)
							->where('entityID', $entityID)
							->get();
				$dailyVisitorsCount = $q3->row()->dspCount - 0;
			}

			$q4 = $this->db->select('count(newVisitorID) as vendors')
							->from('newVisitors')
							->where('type','Vendor')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$vendors = $q4->row()->vendors - 0;

			$q5 = $this->db->select('count(newVisitorID) as guests')
							->from('newVisitors')
							->where('type','Guest')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$guests = $q5->row()->guests - 0;

			$q6 = $this->db->select('count(newVisitorID) as prelunch')
							->from('newVisitors')
							->where('intime <= ','13:00:00')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$prelunch = $q6->row()->prelunch - 0;

			$q7 = $this->db->select('count(newVisitorID) as postlunch')
							->from('newVisitors')
							->where('intime >= ','13:00:00')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$postlunch = $q7->row()->postlunch - 0;

			$q8 = $this->db->select('count(newVisitorID) as pConference')
							->from('newVisitors')
							->where('purposeOfMeet', 'Conference')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pConference = $q8->row()->pConference - 0;

			$q9 = $this->db->select('count(newVisitorID) as pGovernment')
							->from('newVisitors')
							->where('purposeOfMeet', 'Government')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pGovernment = $q9->row()->pGovernment - 0;

			$q10 = $this->db->select('count(newVisitorID) as pInterview')
							->from('newVisitors')
							->where('purposeOfMeet', 'Interview')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pInterview = $q10->row()->pInterview - 0;

			$q11 = $this->db->select('count(newVisitorID) as pOfficial')
							->from('newVisitors')
							->where('purposeOfMeet', 'Official')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pOfficial = $q11->row()->pOfficial - 0;

			$q12 = $this->db->select('count(newVisitorID) as pPersonal')
							->from('newVisitors')
							->where('purposeOfMeet', 'Personal')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pPersonal = $q12->row()->pPersonal - 0;

			$q13 = $this->db->select('count(newVisitorID) as pPromotional')
							->from('newVisitors')
							->where('purposeOfMeet', 'Promotional Activity')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pPromotional = $q13->row()->pPromotional - 0;

			$q14 = $this->db->select('count(newVisitorID) as pSocial')
							->from('newVisitors')
							->where('purposeOfMeet', 'Social')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pSocial = $q14->row()->pSocial - 0;

			$q15 = $this->db->select('count(newVisitorID) as pVendor')
							->from('newVisitors')
							->where('purposeOfMeet', 'Vendor')
							->where('date >= ',$startDate)
							->where('date <= ',$endDate)
							->where('entityID',$entityID)
							->get();

			$pVendor = $q15->row()->pVendor - 0;

			$result['prebookCount'] = $prebookCount;
			$result['newVisitorCount'] = $newVisitorCount;
			$result['dailyVisitorCount'] = $dailyVisitorsCount;
			$result['vendors'] = $vendors;
			$result['guests'] = $guests;
			$result['prelunch'] = $prelunch;
			$result['postlunch'] = $postlunch;
			$result['pConference'] = $pConference;
			$result['pGovernment'] = $pGovernment;
			$result['pInterview'] = $pInterview;
			$result['pOfficial'] = $pOfficial;
			$result['pPersonal'] = $pPersonal;
			$result['pPromotional'] = $pPromotional;
			$result['pSocial'] = $pSocial;
			$result['pVendor'] = $pVendor;

			echo json_encode($result, JSON_FORCE_OBJECT);
		}
	}

	public function getVisitors($sessionID, $startDate, $endDate, $visitorName){

		// $startDate = '2017-10-10';
		// $endDate = '2018-10-10';
		// $sessionID = 'pbl9abbqs6fcdr67vate05l0dus9iqqe';
		// $visitorName = 'ash';


		$q = $this->db->select('adminID, entityID')
						->from('adminAccts')
						->where('sessionID', $sessionID)
						->get();

		$adminID = $q->row()->adminID;
		$entityID = $q->row()->entityID;

		if($adminID){

			$q1 = $this->db->select('visitorName, newVisitorID')
							->from('newvisitors')
							->where("(visitorName LIKE '".$visitorName."%')")
							->where('date >= ',$startDate)
							->where('date < ',$endDate)
							->where('entityID', $entityID)
							->order_by('LENGTH(visitorName)')
							->limit(10, 0)
							->get();

			$result = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('newVisitors'=>$result,'num'=>$num),JSON_FORCE_OBJECT);

		}

	}

	public function getVisitorLogs($sessionID, $visitorID, $startDate, $endDate){

		// $startDate = '2017-10-10';
		// $endDate = '2018-10-10';
		// $sessionID = '1grortji78g40ptsegnpn548k67h4l11';
		// $visitorID = 180;

		$q = $this->db->select('adminID, entityID')
						->from('adminAccts')
						->where('sessionID', $sessionID)
						->get();

		$adminID = $q->row()->adminID;
		$entityID = $q->row()->entityID;

		if($adminID){

			if($visitorID){

				$q1 = $this->db->select('mobNum')
								->from('newVisitors')
								->where('newVisitorID',$visitorID)
								->get();

				$mobNum = $q1->row()->mobNum;

				$q2 = $this->db->select('visitorName, userName, userID, inTime, outTime, date, newVisitorID')
								->from('newVisitors')
								->where('mobNum', $mobNum)
								->where('date >= ', $startDate)
								->where('date <= ', $endDate)
								->where('entityID', $entityID)
								->get();

				$result = $q2->result();

				$num = $q2->num_rows();

			}else{
				$q2 = $this->db->select('visitorName, userName, userID, inTime, outTime, date, newVisitorID')
								->from('newVisitors')
								->where('date >= ', $startDate)
								->where('date <= ', $endDate)
								->where('entityID', $entityID)
								->get();

				$result = $q2->result();

				$num = $q2->num_rows();
			}	

			echo json_encode(array('result'=>$result, 'num'=>$num), JSON_FORCE_OBJECT);
		}
	}
}

 ?>