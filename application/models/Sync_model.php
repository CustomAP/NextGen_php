<?php

class Sync_model extends CI_Model{

	public function getDepartments($entityID){
		// $entityID=1;

		$q=$this->db->select('*')
					->from('departments')
					->where('entityID',$entityID)
					->get();

		$result=$q->result();

		$num=$q->num_rows();

		echo json_encode(array('departments'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function getEmployees($sessionID,$IDMax){

		// $IDMax=60;
		// $sessionID='3265f57cf81e66089671bfe0f38efea417384270';

		$q=$this->db->select('entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$entityID=$q->row()->entityID;

		$q1=$this->db->select('userID as employeeID,userName as employeeName,departments.departmentID,userID,departments.departmentName')
					->from('useraccts')
					->where('useraccts.entityID',$entityID)
					->where('useraccts.userID > ',$IDMax)
					->join('departments','useraccts.departmentID = departments.departmentID','left')
					->get();

		$result=$q1->result();

		$num=$q1->num_rows();

		echo json_encode(array('employees'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function getResidents($sessionID,$IDMax){

		// $IDMax=60;
		// $sessionID='2qhe9ddg8irdk6ie05dsl6do54c1d9of';

		$q=$this->db->select('entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$entityID=$q->row()->entityID;

		$q1=$this->db->select('userID as residentID,userName as residentName, flatNum, wing')
					->from('useraccts')
					->where('useraccts.entityID',$entityID)
					->where('useraccts.userID > ',$IDMax)
					->join('departments','useraccts.departmentID = departments.departmentID','left')
					->get();

		$result=$q1->result();

		$num=$q1->num_rows();

		echo json_encode(array('employees'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
	}
}