<?php

class DSP_model extends CI_Model{
	public function addDSP($name, $address, $spouseName, $IDProof, $IDNum, $mobileNum, $image, $sessionID, $spouseOccupation, $familyMembers){
		// $sessionID='ed7qljnq476nddldilpq71kkial97qj4';

		$q=$this->db->select('securityAcctID, entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securityAcctID=$q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		if($securityAcctID){
			$data=array(
				'name'=>$name,
				'IDProof'=>$IDProof,
				'IDNum'=>$IDNum,
				'mobNum'=>$mobileNum,
				'address'=>$address,
				'spouse'=>$spouseName,
				'entityID'=>$entityID,
				'spouseOccupation' => $spouseOccupation,
				'familyMembers' => $familyMembers
				);

			$q1=$this->db->insert('dsp',$data);

			$id=$this->db->insert_id();

			/*
				saving image
			*/
			$decodedImage=base64_decode($image);
			file_put_contents('./dsp/'.$id.".JPG", $decodedImage);

			echo json_encode(array('dspID'=>$id));
		}
	}

	public function getDSP($sessionID, $IDMax){
		// $sessionID='dbogpmhqbo5qjil6omiso7rhfs80hmrp';
		// $IDMax = 0;

		$securityAcctID = 0;
		$apartmentID = 0;

		$q = $this->db->select('securityAcctID, entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		if($q->num_rows()){

			$securityAcctID = $q->row()->securityAcctID;
			$entityID = $q->row()->entityID;
		}

		$q2 = $this->db->select('userID , entityID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		if($q2->num_rows()){
			$userID = $q2->row()->userID;
			$apartmentID = $q2->row()->entityID;
		}

		if($securityAcctID){
			$q1 = $this->db->select('*')
							->from('dsp')
							->where('entityID',$entityID)
							->where('dspID > ',$IDMax)
							->get();

			$dsp = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dsp'=>$dsp, 'num'=>$num), JSON_FORCE_OBJECT);
		}else if($userID){
			$q1 = $this->db->select('*')
							->from('dsp')
							->where('entityID',$apartmentID)
							->where('dspID > ',$IDMax)
							->get();

			$dsp = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dsp'=>$dsp, 'num'=>$num), JSON_FORCE_OBJECT);
		}
	}

	public function inDSP($sessionID,$dspID){

		// $sessionID='ed7qljnq476nddldilpq71kkial97qj4';
		// $dspID=2;

		$q=$this->db->select('securityAcctID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$securityAcctID=$q->row()->securityAcctID;

		if($securityAcctID){
			/*
				getting date and in-time
			*/
			date_default_timezone_set('Asia/Kolkata');

			$date = date('Y-m-d', time());
			$inTime =date('H:i:s',time());
			
			$data=array(
				'dspID'=>$dspID,
				'date'=>$date,
				'inTime'=>$inTime
				);

			$q1=$this->db->insert('dspentry',$data);

			$dspEntryID=$this->db->insert_id();

			$this->load->model('Notifications_model');
			$this->Notifications_model->sendDSPPushNotif($dspID);

			echo json_encode(array('dspEntryID'=>$dspEntryID,'time'=>$inTime),JSON_FORCE_OBJECT);
		}
	}


	public function outDSP($sessionID,$dspEntryID){

		// $sessionID='ed7qljnq476nddldilpq71kkial97qj4';
		// $dspEntryID=1;

		$q=$this->db->select('securityAcctID')
				->from('securityAccts')
				->where('sessionID',$sessionID)
				->get();

		$securityAcctID=$q->row()->securityAcctID;

		if($securityAcctID){
			/*
				getting date and in-time
			*/
			date_default_timezone_set('Asia/Kolkata');

			//$date = date('Y-m-d', time());
			$outTime =date('H:i:s',time());

			$data=array(
				'outTime'=>$outTime
				);

			$q1=$this->db->where('dspEntryID',$dspEntryID)
						->update('dspentry',$data);

			$q2 = $this->db->select('dspID')
							->from('dspentry')
							->where('dspEntryID', $dspEntryID)
							->get();

			$dspID = $q2->row()->dspID;

			$this->load->model('Notifications_model');
			$this->Notifications_model->sendExitDSPNotif($dspID);

			echo json_encode(array('time'=>$outTime),JSON_FORCE_OBJECT);
		}
	}

	public function addDSPLink($sessionID, $dspID, $work){

		$q = $this->db->select('userID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){
			$data = array(
				'residentID' => $userID,
				'dspID' => $dspID,
				'work' => $work
			);

			$q1 = $this->db->insert('dsplink', $data);

			$id=$this->db->insert_id();

			echo json_encode(array('dspLinkID' => $id));
		}
	}

	public function getDSPLink($sessionID, $dspID){

		// $sessionID='ed7qljnq476nddldilpq71kkial97qj4';
		// $dspID = 2;

		$q = $this->db->select('securityAcctID')
						->from('securityAccts')
						->where('sessionID', $sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;

		if($securityAcctID){
			$q1 = $this->db->select('dsplink.*, useraccts.userID, useraccts.userName, useraccts.flatNum, useraccts.wing')
							->from('dsplink')
							->join('useraccts', 'useraccts.userID = dsplink.residentID')
							->where('dspID', $dspID)
							->get();

			$result = $q1->result();

			$num = $q1->num_rows();

			echo json_encode(array('dspLinks'=> $result, 'num'=> $num), JSON_FORCE_OBJECT);
		}
	}

	public function getDSPLog($sessionID){

		// $sessionID='ed7qljnq476nddldilpq71kkial97qj4';

		$q = $this->db->select('securityAcctID, entityID')
						->from('securityAccts')
						->where('sessionID',$sessionID)
						->get();

		$securityAcctID = $q->row()->securityAcctID;
		$entityID = $q->row()->entityID;

		date_default_timezone_set('Asia/Kolkata');
		$date = date('Y-m-d', time());

		if($securityAcctID){
			$q1 = $this->db->select('dspentry.*, dsp.name, dsp.entityID')
							->from('dspentry')
							->join('dsp','dspentry.dspID = dsp.dspID', 'left')
							->where('dsp.entityID', $entityID)
							->where('dspentry.date',$date)
							->get();

			$dailyvisitorLogs = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dspLog'=>$dailyvisitorLogs, 'num'=>$num),JSON_FORCE_OBJECT);
		}
	}

	public function getAllDSPLinksForResident($sessionID){

		// $sessionID = 'i4usg4fumamoaatom9ftt7nioj972iaf';

		$q = $this->db->select('userID')
						->from('useraccts')
						->where('sessionID', $sessionID)
						->get();

		$userID = $q->row()->userID;

		if($userID){
			$q1 = $this->db->select('dsplink.* , dsp.name')
							->from('dsplink')
							->join('dsp', 'dsplink.dspID = dsp.dspID', 'inner')
							->where('dsplink.residentID', $userID)
							->get();

			$result = $q1->result();
			$num = $q1->num_rows();

			echo json_encode(array('dspLinks' => $result, 'num' => $num), JSON_FORCE_OBJECT);
		}
	}
}