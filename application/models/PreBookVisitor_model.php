<?php

class PreBookVisitor_model extends CI_Model {

	public function fetchPreBookVisitorList($IDMax, $sessionID) {

		//  $sessionID='3kggk7hu0aph72hae54u89grmfm7rfqe';
		// $IDMax=1;

		date_default_timezone_set('Asia/Kolkata');

		$date = date('Y-m-d', time());
		// echo $date;		
		//$date='2017-11-06';

		$q1=$this->db->select('entityID')
					->from('securityAccts')
					->where('sessionID',$sessionID)
					->get();

		$entityID=$q1->row()->entityID;

		$q=$this->db->select('*')
					->from('prebook')
					->where('prebook.prebookID > ',$IDMax)
					->where('prebook.date',$date)
					->where('prebook.entityID',$entityID)
					->join('useraccts','prebook.userID = useraccts.userID','inner')
					->join('departments','useraccts.departmentID = departments.departmentID','left')
					->get();

		$result = $q->result();
		$num=$q->num_rows();

		echo json_encode(array('PreBookList'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function sendPreBookRequest($sessionID,$visitorName,$purposeOfMeet,$organisation,$date,$time,$IDProof,$IDNum,$mobNum,$type,$visitorID){

		// $sessionID='6003da5109f54c4c490eefb12bae1e8cddac9577';
		// $visitorName='ash';
		// $purposeOfMeet='Meeting';
		// $organisation='self';
		// $date='2017-10-26';
		// $time='23:09';
		// $IDProof='Adhar Card';
		// $IDNum='dfafa';
		// $type='Guest';
		// $mobNum='7894561223';

		/*
			getting employee ID, company ID
		*/
		$q=$this->db->select('userID,entityID')
					->from('useraccts')
					->where('sessionID',$sessionID)
					->get();

		$employeeAcctID=$q->row();
		$userID=$q->row()->userID;
		$entityID=$q->row()->entityID;

		// print_r($sessionID);
		// print_r($employeeAcctID);

		/*
			if employee ID exists
		*/
		if($employeeAcctID){

			/*
				array of data to be inserted
			*/
			$data=array(
				'visitorName'=>$visitorName,
				'purposeOfMeet'=>$purposeOfMeet,
				'organisation'=>$organisation,
				'date'=>$date,
				'time'=>$time,
				'IDProof'=>$IDProof,
				'IDNo'=>$IDNum,
				'mobNum'=>$mobNum,
				'type'=>$type,
				'entityID'=>$entityID,
				'userID'=>$userID,
				'visitorID'=>$visitorID
				);

			$q1=$this->db->insert('prebook',$data);

			$prebookID=$this->db->insert_id();

			echo json_encode(array('prebookID'=>$prebookID));
		}
	}

	public function getPreBookItemsToDelete($sessionID){

		// $sessionID='56fa20baeab1d91178a7f7f3657ed22eeaac82dd';

		$q=$this->db->select('userID')
					->from('useraccts')
					->where('sessionID',$sessionID)
					->get();

		$userID=$q->row()->userID;

		if($userID){
			$q=$this->db->select('prebookID')
						->from('prebook')
						->where('userID',$userID)
						->where('visitorID = 0')
						->get();

			$result=$q->result();

			$num=$q->num_rows();

			echo json_encode(array('prebookIDs'=>$result,'num'=>$num),JSON_FORCE_OBJECT);
		}
	}

}