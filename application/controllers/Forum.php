<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Forum_model');
	}


	public function getForumItems(){
		$sessionID = $this->input->post('sessionID');

		$this->Forum_model->getForumItems($sessionID);
	}

	public function addForumItem(){
		$sessionID = $this->input->post('sessionID');
		$heading = $this->input->post('heading');
		$description = $this->input->post('description');
		$poll = $this->input->post('poll');
		$option1 = $this->input->post('option1');
		$option2 = $this->input->post('option2');

		$this->Forum_model->addForumItem($sessionID, $heading, $description, $poll, $option1, $option2);
	}

	public function vote(){
		$sessionID = $this->input->post('sessionID');
		$forumID = $this->input->post('forumID');
		$optionNum = $this->input->post('optionNum');

		$this->Forum_model->vote($sessionID, $forumID, $optionNum);
	}

	public function getPollResult(){
		$sessionID = $this->input->post('sessionID');
		$forumID = $this->input->post('forumID');

		$this->Forum_model->getPollResult($sessionID, $forumID);
	}
}

?>