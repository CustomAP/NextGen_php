<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
	}


	public function getStatistics(){
		$sessionID = $this->input->post('sessionID');
		$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');

		$this->Admin_model->getStatistics($sessionID, $startDate, $endDate);
	}

	public function getVisitors(){
		$sessionID = $this->input->post('sessionID');
		$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');
		$visitorName = $this->input->post('visitorName');

		$this->Admin_model->getVisitors($sessionID, $startDate, $endDate, $visitorName);
	}

	public function getVisitorLogs(){
		$sessionID = $this->input->post('sessionID');
		$visitorID = $this->input->post('visitorID');
		$startDate = $this->input->post('startDate');
		$endDate = $this->input->post('endDate');

		$this->Admin_model->getVisitorLogs($sessionID, $visitorID, $startDate, $endDate);
	}
}