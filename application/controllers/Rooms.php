<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Rooms_model');
	}

	public function getRooms(){
		$sessionID = $this->input->post('sessionID');

		$this->Rooms_model->getRooms($sessionID);
	}

	public function getRoomBookings(){
		$sessionID = $this->input->post('sessionID');
		$date = $this->input->post('date');
		$roomID = $this->input->post('roomID');

		$this->Rooms_model->getRoomBookings($sessionID, $date, $roomID);
	}

	public function bookRoom(){
		$sessionID = $this->input->post('sessionID');
		$date = $this->input->post('date');
		$roomID = $this->input->post('roomID');
		$startTime = $this->input->post('startTime');
		$endTime = $this->input->post('endTime');

		$this->Rooms_model->bookRoom($sessionID, $date, $roomID, $startTime, $endTime);
	}
}