<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Otp extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Otp_model');
	}

	public function sendOTP(){
		$mobNum = $this->input->post('mobNum');

		$this->Otp_model->sendOTP($mobNum);
	}

	public function verifyOTP() {
		$otp = $this->input->post('OTP');
		$sessionID = $this->input->post('sessionID');

		$this->Otp_model->verifyOTP($otp,$sessionID);
	}
}