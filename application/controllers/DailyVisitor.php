<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DailyVisitor extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('DailyVisitor_model');
	}

	public function addDailyVisitor(){
		$name=$this->input->post('name');
		$organisation=$this->input->post('organisation');
		$designation=$this->input->post('designation');
		$IDProof=$this->input->post('IDProof');
		$IDNum=$this->input->post('IDNum');
		$mobileNum=$this->input->post('mobileNum');
		$image=$this->input->post('image');
		$sessionID=$this->input->post('sessionID');

		$this->DailyVisitor_model->addDailyVisitor($name,$organisation,$designation,$IDProof,$IDNum,$mobileNum,$image,$sessionID);
	}

	public function inDailyVisitor(){
		$sessionID=$this->input->post('sessionID');
		$dailyVisitorID=$this->input->post('dailyVisitorID');

		$this->DailyVisitor_model->inDailyVisitor($sessionID,$dailyVisitorID);
	}

	public function outDailyVisitor(){
		$sessionID=$this->input->post('sessionID');
		$dailyVisitorEntryID=$this->input->post('dailyVisitorEntryID');

		$this->DailyVisitor_model->outDailyVisitor($sessionID,$dailyVisitorEntryID);
	}

	public function getDailyVisitors(){
		$sessionID = $this->input->post('sessionID');
		$IDMax = $this->input->post('IDMax');

		$this->DailyVisitor_model->getDailyVisitors($sessionID, $IDMax);
	}

	public function getDailyVisitorLogs(){
		$sessionID = $this->input->post('sessionID');

		$this->DailyVisitor_model->getDailyVisitorLogs($sessionID);
	}
}