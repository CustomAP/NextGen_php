<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PreBookVisitor extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('PreBookVisitor_model');
	}

	public function fetchPreBookVisitorList(){
		$IDMax=$this->input->post('IDMax');
		$sessionID=$this->input->post('sessionID');

		$this->PreBookVisitor_model->fetchPreBookVisitorList($IDMax,$sessionID);
	}

	public function sendPreBookRequest(){
		$sessionID=$this->input->post('sessionID');
		$visitorName=$this->input->post('visitorName');
		$purposeOfMeet=$this->input->post('purposeOfMeet');
		$organisation=$this->input->post('organisation');
		$date=$this->input->post('date');
		$time=$this->input->post('time');
		$IDProof=$this->input->post('IDProof');
		$IDNum=$this->input->post('IDNum');
		$mobNum=$this->input->post('mobNum');
		$type=$this->input->post('type');
		$visitorID=$this->input->post('visitorID');

		// echo 'test '.$sessionID.' '.$visitorID;
		// var_dump($_POST);

		$this->PreBookVisitor_model->sendPreBookRequest($sessionID,$visitorName,$purposeOfMeet,$organisation,$date,$time,$IDProof,$IDNum,$mobNum,$type,$visitorID);
	}

	public function getPreBookItemsToDelete(){
		$sessionID=$this->input->post('sessionID');

		$this->PreBookVisitor_model->getPreBookItemsToDelete($sessionID);
	}
}