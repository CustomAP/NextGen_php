<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DSP extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('DSP_model');
	}

	public function addDSP(){
		$name=$this->input->post('name');
		$address=$this->input->post('address');
		$spouseName=$this->input->post('spouseName');
		$IDProof=$this->input->post('IDProof');
		$IDNum=$this->input->post('IDNum');
		$mobileNum=$this->input->post('mobileNum');
		$image=$this->input->post('image');
		$sessionID=$this->input->post('sessionID');
		$spouseOccupation = $this->input->post('spouseOccupation');
		$familyMembers = $this->input->post('familyMembers');

		$this->DSP_model->addDSP($name, $address, $spouseName, $IDProof, $IDNum, $mobileNum, $image, $sessionID, $spouseOccupation, $familyMembers);

	}

	public function getDSP(){
		$sessionID = $this->input->post('sessionID');
		$IDMax = $this->input->post('IDMax');

		$this->DSP_model->getDSP($sessionID, $IDMax);
	}


	public function inDSP(){
		$sessionID=$this->input->post('sessionID');
		$dspID=$this->input->post('dspID');

		$this->DSP_model->inDSP($sessionID,$dspID);
	}

	public function outDSP(){
		$sessionID=$this->input->post('sessionID');
		$dspEntryID=$this->input->post('dspEntryID');

		$this->DSP_model->outDSP($sessionID,$dspEntryID);
	}

	public function addDSPLink(){
		$sessionID = $this->input->post('sessionID');
		$dspID = $this->input->post('dspID');
		$work = $this->input->post('work');

		$this->DSP_model->addDSPLink($sessionID, $dspID, $work);
	}

	public function getDSPLink(){
		$sessionID = $this->input->post('sessionID');
		$dspID = $this->input->post('dspID');

		$this->DSP_model->getDSPLink($sessionID, $dspID);
	}

	public function getDSPLog(){
		$sessionID = $this->input->post('sessionID');

		$this->DSP_model->getDSPLog($sessionID);
	}

	public function getAllDSPLinksForResident(){
		$sessionID = $this->input->post('sessionID');

		$this->DSP_model->getAllDSPLinksForResident($sessionID);
	}
}