<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class NewVisitor extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('NewVisitor_model');
	}

	public function uploadVisitorDetails(){
		$visitorName=$this->input->post('visitorName');
		$visitorType=$this->input->post('visitorType');
		$purposeOfMeet=$this->input->post('purposeOfMeet');
		$organisation=$this->input->post('organisation');
		$IDProof=$this->input->post('IDProof');
		$IDNum=$this->input->post('IDNum');
		$userName=$this->input->post('userName');
		$userID=$this->input->post('userID');
		$employeeDeptID=$this->input->post('employeeDeptID');
		$visitorImage=$this->input->post('visitorImage');
		$mobNum=$this->input->post('mobNum');
		$sessionID=$this->input->post('sessionID');
		$prebookID=$this->input->post('prebookID');
		$IDImage = $this->input->post('IDImage');
		$wing = $this->input->post('wing');
		$flatNum = $this->input->post('flatNum');

		$this->NewVisitor_model->uploadVisitorDetails($visitorName,$visitorType,$purposeOfMeet,$organisation,$IDProof,$IDNum,$userName,$userID,$employeeDeptID,$mobNum,$visitorImage,$sessionID,$prebookID,$IDImage, $wing, $flatNum);
	}

	public function exit(){
		$visitorID=$this->input->post('visitorID');
		$sessionID=$this->input->post('sessionID');

		$this->NewVisitor_model->exit($visitorID,$sessionID);
	}

	public function fetchNewVisitors(){
		$sessionID=$this->input->post('sessionID');
		$IDMax=$this->input->post('IDMax');

		$this->NewVisitor_model->fetchNewVisitors($sessionID,$IDMax);
	}

	public function exitVisitorByEmployee(){
		$sessionID = $this->input->post('sessionID');
		$visitorID = $this->input->post('visitorID');

		$this->NewVisitor_model->exitVisitorByEmployee($sessionID, $visitorID);
	}

	public function checkExited(){
		$sessionID = $this->input->post('sessionID');
		$visitorID = $this->input->post('visitorID');

		$this->NewVisitor_model->checkExited($sessionID, $visitorID);
	}

	public function getNewVisitorsForSecurity(){
		$sessionID = $this->input->post('sessionID');

		$this->NewVisitor_model->getNewVisitorsForSecurity($sessionID);
	}

	public function checkRepeatedVisitor(){
		$sessionID = $this->input->post('sessionID');
		$mobNum = $this->input->post('mobileNum');

		$this->NewVisitor_model->checkRepeatedVisitor($sessionID, $mobNum);
	}
}