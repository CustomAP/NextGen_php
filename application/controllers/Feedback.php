<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Feedback_model');
	}

	public function sendFeedback(){
		$sessionID = $this->input->post('sessionID');
		$feedback = $this->input->post('feedback');

		$this->Feedback_model->sendFeedback($sessionID, $feedback);
	}

}

?>