<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PhoneDirectory extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('PhoneDirectory_model');
	}


	public function addDirectory(){
		$adminSessionID = $this->input->post('adminSessionID');
		$wing = $this->input->post('wing');
		$flatNum = $this->input->post('flatNum');
		$name = $this->input->post('name');
		$mobNum = $this->input->post('mobileNum');

		$this->PhoneDirectory_model->addDirectory($adminSessionID, $wing, $flatNum, $name, $mobNum);
	}

	public function getDirectory(){
		$sessionID = $this->input->post('sessionID');

		$this->PhoneDirectory_model->getDirectory($sessionID);
	}
}