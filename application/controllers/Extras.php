<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Extras extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Notifications_model');
	}

	public function sos(){
		$sessionID = $this->input->post('sessionID');

		$this->Notifications_model->sos($sessionID);

		echo json_encode(array('result' => 0));
	}

}