<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Parking extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Parking_model');
	}

	public function updateCount(){
		$sessionID = $this->input->post('sessionID');
		$count = $this->input->post('count');

		$this->Parking_model->updateCount($sessionID, $count);
	}


	public function getCounter(){
		$sessionID = $this->input->post('sessionID');

		$this->Parking_model->getCounter($sessionID);
	}
}