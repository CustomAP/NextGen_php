<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utilities extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Utilities_model');
	}


	public function addUtility(){
		$sessionID = $this->input->post('sessionID');
		$utilityName = $this->input->post('utilityName');
		$utilityType = $this->input->post('utilityType');
		$mobileNum = $this->input->post('mobileNum');

		$this->Utilities_model->addUtility($sessionID, $utilityName, $utilityType, $mobileNum);
	}

	public function getUtilities(){
		$sessionID = $this->input->post('sessionID');

		$this->Utilities_model->getUtilities($sessionID);
	}

	public function getOrders(){
		$sessionID = $this->input->post('sessionID');
		$utilityID = $this->input->post('utilityID');

		$this->Utilities_model->getOrders($sessionID, $utilityID);
	}

	public function addOrder(){
		$sessionID = $this->input->post('sessionID');
		$paymentMethod = $this->input->post('paymentMode');
		$orderType = $this->input->post('orderType');
		$orderPhoto = $this->input->post('orderImage');
		$utilityID = $this->input->post('utilityID');

		$this->Utilities_model->addOrder($sessionID, $paymentMethod, $orderType, $orderPhoto, $utilityID);
	}

	public function getOrdersForUtility(){
		$sessionID = $this->input->post('sessionID');

		$this->Utilities_model->getOrdersForUtility($sessionID);
	}

	public function uploadAmountForOrder(){
		$sessionID = $this->input->post('sessionID');
		$orderID = $this->input->post('orderID');
		$amount = $this->input->post('amount');

		$this->Utilities_model->uploadAmountForOrder($sessionID, $orderID, $amount);
	}

	public function changeMobNum(){
		$sessionID = $this->input->post('sessionID');
		$mobNum = $this->input->post('mobNum');

		$this->Utilities_model->changeMobNum($sessionID, $mobNum);
	}

	public function changeSecondaryMobNum(){
		$sessionID = $this->input->post('sessionID');
		$secondaryMobNum = $this->input->post('secondaryMobNum');

		$this->Utilities_model->changeSecondaryMobNum($sessionID, $secondaryMobNum);
	}
}