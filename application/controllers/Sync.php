<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Sync extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Sync_model');
	}

	public function getDepartments(){
		$companyID=$this->input->post('companyID');

		$this->Sync_model->getDepartments($companyID);
	}

	public function getEmployees(){
		$sessionID=$this->input->post('sessionID');
		$IDMax=$this->input->post('IDMax');

		$this->Sync_model->getEmployees($sessionID,$IDMax);
	}

	public function getResidents(){
		$sessionID=$this->input->post('sessionID');
		$IDMax=$this->input->post('IDMax');

		$this->Sync_model->getResidents($sessionID,$IDMax);
	}
}