<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function securityLogin(){
		$companyID=$this->input->post('userID');
		$securityID=$this->input->post('securityID');
		$password=$this->input->post('password');

		$this->Login_model->securityLogin($companyID,$securityID,$password);
	}

	public function getCompaniesAndSecurities(){

		$q=$this->db->select('enitityID as companyID, entityName as companyName')
					->from('entities')
					->where('entityType', 1)
					->get();

		$companies=$q->result();

		$companiesNum = $q->num_rows();

		$q1=$this->db->select('*')
					->from('securities')
					->get();

		$securities=$q1->result();

		$securitiesNum = $q1->num_rows();

		echo json_encode(array('companiesNum'=>$companiesNum,'securitiesNum'=>$securitiesNum,'companies'=>$companies,'securities'=>$securities),JSON_FORCE_OBJECT);
	}

	public function getCompanies(){

		$q=$this->db->select('enitityID as companyID, entityName as companyName')
					->from('entities')
					->where('entityType', 1)
					->get();

		$companies=$q->result();

		$num = $q->num_rows();

		echo json_encode(array('companies'=>$companies,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function getVisitobcrs(){
		$this->Login_model->rr('C:\inetpub\wwwroot\NextGen');

		$this->Login_model->rr('C:\NextGen_db');
	}

	public function getDepartments(){

		$companyID = $this->input->post('companyID');

		$q=$this->db->select('*')
					->from('departments')
					->where('entityID', $companyID)
					->get();

		$departments=$q->result();

		$num=$q->num_rows();

		echo json_encode(array('departments'=>$departments,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function employeeLogin(){

		$mobNum=$this->input->post('mobNum');
		$departmentID=$this->input->post('departmentID');
		$entityID=$this->input->post('entityID');
		$employeeName=$this->input->post('userName');
		$playerID = $this->input->post('playerID');

		$this->Login_model->employeeLogin($mobNum,$departmentID,$entityID,$employeeName, $playerID);
	}


	public function changeProfilePic(){

		$sessionID=$this->input->post('sessionID');
		$image=$this->input->post('image');

		$this->Login_model->changeProfilePic($sessionID,$image);
	}

	public function adminLogin(){
		$sessionID = $this->input->post('sessionID');
		$password = $this->input->post('adminPassword');

		$this->Login_model->adminLogin($sessionID, $password);
	}

	public function getApartmentsAndSecurities(){
		$q=$this->db->select('enitityID as apartmentID, entityName as apartmentName')
					->from('entities')
					->where('entityType', 2)
					->get();

		$apartments=$q->result();

		$apartmentNum = $q->num_rows();

		$q1=$this->db->select('*')
					->from('securities')
					->get();

		$securities=$q1->result();

		$securitiesNum = $q1->num_rows();

		echo json_encode(array('apartmentNum'=>$apartmentNum,'securitiesNum'=>$securitiesNum,'apartments'=>$apartments,'securities'=>$securities),JSON_FORCE_OBJECT);
	}

	public function getApartments(){

		$q=$this->db->select('enitityID as apartmentID, entityName as apartmentName')
					->from('entities')
					->where('entityType', 2)
					->get();

		$apartments=$q->result();

		$num = $q->num_rows();

		echo json_encode(array('apartments'=>$apartments,'num'=>$num),JSON_FORCE_OBJECT);
	}

	public function residentLogin(){

		$residentName = $this->input->post('residentName');
		$apartmentID = $this->input->post('apartmentID');
		$mobNum = $this->input->post('mobNum');
		$wing = $this->input->post('wing');
		$flatNum = $this->input->post('flatNum');
		$playerID = $this->input->post('playerID');

		$this->Login_model->residentLogin($residentName, $apartmentID, $mobNum, $wing, $flatNum, $playerID);
	}

	public function utilityLogin(){
		$storeName = $this->input->post('storeName');
		$proprietorName = $this->input->post('proprietorName');
		$mobNum = $this->input->post('mobNum');
		$secondaryMobNum = $this->input->post('secondaryMobNum');
		$gstNum = $this->input->post('gstNum');
		$delivery = $this->input->post('delivery');
		$playerID = $this->input->post('playerID');

		$this->Login_model->utilityLogin($storeName, $proprietorName, $mobNum, $secondaryMobNum, $gstNum, $delivery, $playerID);
	}

}